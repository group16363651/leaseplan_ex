package com.starter.stepdefinition;

import com.starter.Utility;
import com.starter.api.SearchProductAPI;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.hamcrest.core.Is;

import static com.starter.Utility.LAST_API_RESPONSE;

public class SearchProduct {
    private SearchProductAPI searchProductAPI;

    public SearchProduct() {
        this.searchProductAPI = new SearchProductAPI();
    }

    @Given("user searches product of {string}")
    public void callSearchProduct(String product) {
        Utility.set(LAST_API_RESPONSE, this.searchProductAPI.get(product));
    }

    @Then("response status code is {int}")
    public void verifyResponseStatusCode(int expectedStatusCode) {
        Utility.getLastApiResponse().then().assertThat().statusCode(expectedStatusCode);
    }

    @Then("response shows error {string}")
    public void verifyResponseErrorMessage(String expectedErrorMessage) {
        Utility.getLastApiResponse().then().assertThat()
                .body("detail.error", Is.is(true));
        Utility.getLastApiResponse().then().assertThat()
                .body("detail.message", Is.is(expectedErrorMessage));
    }
}
