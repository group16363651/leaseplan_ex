package com.starter;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features/search",
        glue = {"com.starter.stepdefinition"},
        plugin = { "pretty", "html:target/cucumber-reports/cucumber-report.html"},
        monochrome = true
)
public class TestRunner {
}
