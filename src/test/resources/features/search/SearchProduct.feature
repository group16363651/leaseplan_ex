Feature: Search for Products


    Scenario Outline: user has to search the products
      Given user searches product of "<product>"
      Then response status code is 200

      Examples:
        | product |
        | orange  |
        | apple   |
        | pasta   |
        | cola    |


    Scenario: Negative Scenario to search unavailable product
      Given user searches product of "pizza"
      Then response status code is 404
      Then response shows error "Not found"