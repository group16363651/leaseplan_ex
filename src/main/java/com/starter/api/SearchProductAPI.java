package com.starter.api;

import io.restassured.response.Response;

public class SearchProductAPI extends BaseAPI {

    public Response get(String product) {
        return getRequestSpecification()
                .pathParam("product", product.toLowerCase())
                .get("/search/demo/{product}");

    }
}
