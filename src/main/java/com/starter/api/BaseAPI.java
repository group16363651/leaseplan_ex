package com.starter.api;


import com.starter.domain.EnvironmentManager;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseAPI {

    protected static final Logger logger = LoggerFactory.getLogger(BaseAPI.class);

    protected BaseAPI() {
        RestAssured.baseURI = EnvironmentManager.getCurrentEnvironment().getBaseUrl();
        logger.info("Init {} instance for {}", this.getClass().getSimpleName(),
                RestAssured.baseURI);
    }

    protected RequestSpecification getRequestSpecification() {
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .log().all();
    }
}
