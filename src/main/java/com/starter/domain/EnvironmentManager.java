package com.starter.domain;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;

import static java.util.Objects.isNull;

public class EnvironmentManager {
    private final static String ENVIRONMENT_CONFIG_JSON = "environments.json";
    private final static String ENVIRONMENT_OPTION = "env";

    private static final Logger logger = LoggerFactory.getLogger(EnvironmentManager.class);
    private static Environment environment;

    public static Environment getCurrentEnvironment() {
        if (isNull(environment)) {
            synchronized (EnvironmentManager.class) {
                if (isEnvPresent()) {
                    environment = loadEnvironment(System.getProperty(ENVIRONMENT_OPTION));
                } else {
                    throw new RuntimeException("A property 'env' property is not specified, please set -Denv=$\\{value\\}");
                }
            }
        }
        return environment;
    }

    public static boolean isEnvPresent() {
        if (isNull(System.getProperty(ENVIRONMENT_OPTION))) {
            return false;
        }
        return true;
    }

    private static Environment loadEnvironment(String env) {
        logger.info("Load configuration for {} environment", env);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            File jsonFile = new File(Objects.requireNonNull(EnvironmentManager.class
                    .getClassLoader().getResource(ENVIRONMENT_CONFIG_JSON)).toURI());
            return mapper.readValue(mapper.readTree(jsonFile).get(env).toString(),
                    Environment.class);
        } catch (IOException | URISyntaxException e) {
            logger.error("An error occurring during loading the {} for {} environment",
                    ENVIRONMENT_CONFIG_JSON, env);
            throw new RuntimeException(e);
        }
    }
}
