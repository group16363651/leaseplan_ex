# CucumberStarterProject

## Description
This is the test automation project to test Rest service using [BDD Cucumber](https://cucumber.io/) framework. 
The project uses Java program language and an open-source Maven build tool for building and running.
The tests are written in BDD style using Cucumber with using RestAssured to call REST service's endpoints and validate responses.

## Requirements
+ Automate the endpoints inside a working CI/CD pipeline to test the service
+ Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
+ Available products: "orange", "apple", "pasta", "cola"
+ Prepare Positive and negative scenarios

## Installation
In order to execute tests on local you need to install the text items:
* [Install Java 1.8](https://openjdk.org/install/) 
* [Install Maven 3.9.2](https://maven.apache.org/install.html)
* [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Clone the project from Gitlab repository 
```
git clone https://gitlab.soft-industry.com/aqa-tests/natalia-s-aqa-test-task.git
```

The service's endpoints are configured in `environments.json` file under ``src/main/resources`` package
(currently only one `uat` environment with baseUrl is added)
```
{
   "uat": {
      "baseUrl": "https://waarkoop-server.herokuapp.com/api/v1/"
   }
}
```

In order to run Cucumber scenarios apply maven `test` goal from project root directory and 
pass the `env` property, if `env` is not specified the error will be occurred during the run
```
 mvn test -Denv=uat
```

Cucumber has build-in HTML report that is automatically generated under `target/cucumber-reports`


## GitLab pipeline
In order to run test in GitLab go to ``Build -> Pipelines`` and click on ``Run pipeline`` button. 
On the executed pipeline click ``Download artifacts`` to download and view HTML Cucumber report.

___________________________________________________________________
<br/>

## The job done:
1. Structure refactoring and cleanup
   * delete redundant folders and files related to Gradle as Maven build tool is used
   * cleanup .gitignore file
   * refactor maven build file pom.xml, delete unused dependencies and plugins related to Serenity as Cucumber is used
   * rename package ``starter`` to ``com.starter``
   
2. Code restructure and implementation
   * add the next java classes under ``src/main/java``
   ```
      + api/
          + model/
              + ProductDTO.java  - represents the product that is returned by service to deserialize json response to POJO's
          + SearchProductAPI.java - represents the search product service with methods to call endpoints
      + domain/
          + Environment.java - represents the environment configuration loaded from json config file `enfironments.json`;
          + EnvironmentManager.java - is responsible for creating a single instance of Environment object and provides
                                      an access to the environment resourcses during the run Ex: EnvironmentManager.getCurrentEnvironment().getBaseUrl();

   ```
   * add the next files under ``src/main/resources``
   ```
      + environments.json - describes the different environments and their resources
   ```
   * add the next java classes under ``src/test/java``
   ```
      + stepdefinition/
          + CommonAssertions.java - contains an implementation of cucumber steps for common response checks (status code, etc)
          + SearchProductAssertions.java - contains the specific checks of search product response
          + SearchProductDefinition.java - contains an implementation of cucumber steps for calling search product endpoint
      + ScenarioContext.java - is used to keep data and shares data between steps inside the cucumber scenario (for exemple response, any step can get the 
                               last response from context and manipulate with it)
      + ScenarioHook.java - implements the Cucumber 'after' hook to clear Scenario context after each scenario (is executed automatically by cucumber)
      + TestRunner.java - this is JUnit runer and start point to execute the test suite
   ```
   * add the next files under ``src/test/resources``
   ```
      + features/
          + search/
              + SearchProduct.feature - contains the test scenarios: 2 positive scenarios and 1 negative scenarios
   ```
   Scenarios use 2 tags for filtering `@smoke` and `@regression`, they can be specified during the run

   ```mvn test -Denv=uat "-Dcucumber.filter.tags=@regression"```
  